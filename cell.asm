
set_x_cell proc						; expects cx to hold the cell id and sets the corresponding bit in player_one_map
		mov ax, player_one_map
		mov bx, 1
		shl bx, cl
		or ax, bx
		mov player_one_map, ax
		ret 0
set_x_cell endp

set_y_cell proc						; expects cx to hold the cell id and sets the corresponding bit in player_two_map
		mov ax, player_two_map
		mov bx, 1
		shl bx, cl
		or ax, bx
		mov player_two_map, ax
		ret 0
set_y_cell endp

cell_x_y_to_index proc
			push bp
			mov bp, sp

			mov ax, [bp+4+0]
			mov bx, grid_dimensions			; cannot emidiately multiply by constant
			mul bx
			add ax, [bp+4+2]

			pop bp
			ret 2*2
cell_x_y_to_index endp

x_y_to_cell_x_y proc			; expects x, y on the stack. returns cell_x in ax and cell_y in bx
			push bp
			mov bp, sp

			mov cx, cell_width_power

			;; calculate cell_y
			mov ax, [bp+4+0]
			sub ax, start_y
			shr ax, cl
			mov bx, ax

			;; to the same for cell_x
			mov ax, [bp+4+2]
			sub ax, start_x
			shr ax, cl


			pop bp
			ret 2*2
x_y_to_cell_x_y endp
