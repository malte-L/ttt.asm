
zero_asci = 48


decimal_to_digit proc
			push bp
			mov bp, sp

			mov ax, [bp+4+0]
			add al, zero_asci

			pop bp
			ret 2
decimal_to_digit endp





asci__clear_block_x_y proc
			push bp
			mov bp, sp


			mov bx, [bp+4+2]
			mov dx, [bp+4+0]

			mov cx, 0
a_row_loop:
 			mov al, es:[si]

			push bx
			push cx
			mov cx, 8
a_col_loop:
			push dx
			push cx
			push ax

			push bx
			push dx
			push 0
			call draw_x_y_rgb

			pop ax
			pop cx
			pop dx
a_col_loop_end:
			add bx, 1
			dec cx
			jnz a_col_loop

a_row_loop_end:
			inc dx
			pop cx
			pop bx
			inc si
			inc cx
			cmp cx, 8
			jne a_row_loop


			pop bp
			ret 4
asci__clear_block_x_y endp






; x,y,asci
_f_x_y_asci proc
			push bp
			mov bp, sp


char_seg = 0F000h
char_gen = 0FA6Eh
			mov bx, [bp+4+2*2]
			mov dx, [bp+4+2*1]


     	mov ax, char_seg
			mov es, ax

      xor ah, ah
			mov al, [bp+4+0]
			shl ax, 3                       ; 8 rows of 8bit representing coloumn values

			mov si, char_gen
			add si, ax

			mov cx, 0
row_loop:
 			mov al, es:[si]
			; mov al, 0FFh

			push bx
			push cx
			mov cx, 8
col_loop:
			push bx
			MOV BX, 1             ; mask out x position
			SHL BX, CL            ; --
			AND BL, AL    
			pop bx
			jz black			  ;
			push dx
			push cx
			push ax

			push bx
			push dx
			push 3
			call draw_x_y_rgb

			pop ax
			pop cx
			pop dx
			jmp col_loop_end
black:
			push dx
			push cx
			push ax

			push bx
			push dx
			push 0
			call draw_x_y_rgb

			pop ax
			pop cx
			pop dx

col_loop_end:
			add bx, 1
			dec cx
			jnz col_loop

row_loop_end:
			inc dx
			pop cx
			pop bx
			inc si
			inc cx
			cmp cx, 8
			jne row_loop


			pop bp
			ret 3*2
_f_x_y_asci endp




print_x_y_msg_len proc
			push bp
			mov bp, sp


			start_offset_x equ [bp+4+3*2]
			y equ [bp+4+2*2]
			msg equ [bp+4+1*2]
			len equ [bp+4+0]


			mov dx, y



			mov cx, 0
			mov bx, start_offset_x

__lstart:
			push cx
			push bx
			push bp
			push dx



			mov bp, msg
			add bp, cx
			mov ax, ds:[bp]
			xor ah, ah
			push bx
			push dx
			push ax
			call _f_x_y_asci
			
			pop dx
			pop bp
			pop bx
			pop cx

			inc cx
			add bx, 8
			mov ax, [bp+4+0]
			cmp cx, ax
			jne __lstart


			mov ax, bx
			mov bx, y


			pop bp
			ret 3*2
print_x_y_msg_len endp








