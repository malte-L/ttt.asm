is_winner? proc		; no args. relies on next_move_by. returns ax <- 1 if is winner. 
			push bp
			mov bp, sp

			mov ax, next_move_by
			shr ax, 1
			jnc is_y_winner?
; is_x_winner?:
			mov ax, player_one_map
			jmp check_patterns
is_y_winner?:
			mov ax, player_two_map

check_patterns:


			mov cx, 0
__loop_pmatch:
			push ax
			mov bp, offset winner_patterns
			add bp, cx
			add bp, cx
			mov bx, ds:[bp]

			; pattern & player == pattern
			and ax, bx
			cmp ax, bx
			pop ax			; does not change flags
			je is_winner


			inc cx
			cmp cx, 8
			jne __loop_pmatch

			mov ax, 0			; keep this
			jmp is_winner_end
is_winner:
			mov ax, 1
is_winner_end:
			pop bp
			ret 0
is_winner? endp


play_move proc				; cx = x; dx = y

			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop



			push cx
			push dx
			call x_y_to_cell_x_y
			push ax
			push bx
			call cell_x_y_to_index

			mov cx, ax		; set_<x|y>_cell expects cx to hold the cell index

			push ax
			push bx
			mov ax, player_one_map
			or ax, player_two_map
			shr ax, cl
			shr ax, 1
			pop bx
			pop ax
			jc invalid_move

			mov bx, next_move_by
			shr bx, 1
			jnc player_y_move

			call set_x_cell
			call draw_player_x_state

			jmp play_move__end
player_y_move:

			call set_y_cell
			call draw_player_y_state


play_move__end:

			call is_winner?
			shr ax, 1
			jnc no_winner_yet

			mov ax, next_move_by
			shr ax, 1
			jc player_one_won
			jmp player_two_won

no_winner_yet:
			mov bx, next_move_by
			xor bx, 1
			mov next_move_by, bx


			call draw_and_increment_moves_counter


			mov time_left, 9
			mov ax, time_left
			call print_time_left


			mov ax, moves_played
			cmp ax, zero_asci+10		; is tie?
			je tie_ending

invalid_move:




			ret 0
play_move endp





draw_and_increment_moves_counter proc
			push played_moves_x
			push played_moves_y
			mov ax, moves_played
			push ax
			call _f_x_y_asci

			mov ax, moves_played
			inc ax
			mov moves_played, ax

			ret 0
draw_and_increment_moves_counter endp


dec_and_print_time_left proc
			push bp

			cmp word ptr [cs:timer_ticks], ticks_per_second
			jl game_loop


			mov [cs:timer_ticks], 0
			mov ax, time_left
			dec ax
			jnz continue_counting

			; counter is zero
			; who won?
			mov ax, next_move_by
			shr ax, 1
			jc player_two_won
			jmp player_one_won

continue_counting:
			mov time_left, ax

			call print_time_left

			pop bp
			ret 0
dec_and_print_time_left endp


print_time_left proc
			push bp

			add ax, zero_asci
			xor ah, ah
			push 100
			push 50+8+4
			push ax
			call _f_x_y_asci

			pop bp
			ret 0
print_time_left endp



reset_time_left macro
			; mov [cs:timer_ticks], 0
			mov time_left, 10
endm

