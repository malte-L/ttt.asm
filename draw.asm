draw_x_y_rgb PROC 			
			push BP
			mov bp, sp

			; mov ax, 2				;Zeiger aus
			; int 33h


			MOV AH, 0Ch     		;setzen der Konfig fürs schreiben

			mov cx, [bp+4+2*2]
			mov dx, [bp+4+2*1]
			mov al, [bp+4+2*0]
			mov BH, 00h     		;Set Seitennummber ; buffer select?
			int 10h


			mov sp, bp
			pop BP
			ret 2*3
draw_x_y_rgb ENDP


draw_cross_x_y_size proc
			push bp
			mov bp, sp


			mov cx, [bp+4+0] ; size
_draw_loop_lr:
			push cx
			push ax
			push dx


			mov ax, cx
			add ax, [bp+4+2*2] ; add x offset

			mov dx, cx
			add dx, [bp+4+2*1]

			push ax
			push dx
			push 0Fh
			call draw_x_y_rgb

			pop dx
			pop ax
			pop cx
			dec cx
			jnz _draw_loop_lr


			mov cx, [bp+4+0] ; size
_draw_loop_rl:
			push cx
			push ax
			push dx

			mov ax, [bp+4+0] ; start with size
			sub ax, cx
			add ax, [bp+4+2*2] ; add x offset

			mov dx, cx
			add dx, [bp+4+2*1]

			push ax
			push dx
			push 0Fh
			call draw_x_y_rgb


			pop dx
			pop ax
			pop cx
			dec cx
			jnz _draw_loop_rl


			pop bp
			ret 2*3
draw_cross_x_y_size endp


draw_horizontal_line_x_y_length_color proc 
			push bp
			mov bp, sp

			mov ax, [bp+4+3*2]


			mov cx, [bp+4+1*2]
__hori_loop:
			push cx
			push ax
			add cx, ax
			push cx							; x
			mov cx, [bp+4+2*2]  ; y
			push cx
			mov cx, [bp+4+2*0]
			push cx							; color
			call draw_x_y_rgb

			pop ax
			pop cx
			dec cx
			jnz __hori_loop

			mov sp, bp
			pop bp
			ret 3*2
draw_horizontal_line_x_y_length_color endp

draw_vertical_line_x_y_length_color proc 
			push bp
			mov bp, sp

			mov ax, [bp+4+2*2]


			mov cx, [bp+4+2*1]
__vert_loop:
			push cx
			push ax
			mov dx, [bp+4+2*3]  ; y
			push dx
			add cx, ax
			push cx							; y
			mov cx, [bp+4+2*0]
			push cx							; color
			call draw_x_y_rgb

			pop ax
			pop cx
			dec cx
			jnz __vert_loop

			mov sp, bp
			pop bp
			ret 3*2
draw_vertical_line_x_y_length_color endp

draw_rect_x1_y1_x2_y2 proc
			push bp
			mov bp, sp

			; need x1 y1 dx
			mov ax, [bp+4+1*2] ; x2
			mov cx, [bp+4+3*2] ; x1
			push cx						 ; x1
			sub ax, cx				 ; dx
			mov dx, [bp+4+2*2] ; y1
			push dx
			push ax
			push 0Fh
			call draw_horizontal_line_x_y_length_color

			; need  x1 y2 dx
			mov dx, [bp+4+3*2]
			push dx
			mov dx, [bp+4+0*2]
			push dx
			mov dx, [bp+4+1*2]
			sub dx, [bp+4+3*2]
			push dx
			push 0Fh
			call draw_horizontal_line_x_y_length_color

			; need x1 y1 dy
			mov dx, [bp+4+3*2]
			push dx
			mov dx, [bp+4+2*2]
			push dx
			mov dx, [bp+4+0*2]
			sub dx, [bp+4+2*2]
			push dx
			push 0Fh
			call draw_vertical_line_x_y_length_color

			; need x2 y1 dy
			mov dx, [bp+4+1*2]
			push dx
			mov dx, [bp+4+2*2]
			push dx
			mov dx, [bp+4+0*2]
			sub dx, [bp+4+2*2]
			push dx
			push 0Fh
			call draw_vertical_line_x_y_length_color

			mov sp, bp
			pop bp
			ret 4*2
draw_rect_x1_y1_x2_y2 endp


__calc_x_offset proc			; returns x offset in ax. expects cx to hold cell id
			push cx
			mov ax, cell_width
			mov bp, cx
			shl bp, 1
			add bp, offset index_to_x_coor_table
			mov cx, ds:[bp]
			mul cx
			add ax, start_x+padding
			pop cx

			ret 0
__calc_x_offset endp

__calc_y_offset proc			; returns y offset in ax. expects cx to hold cell id
			push cx
			mov ax, cell_width
			mov bp, cx
			shl bp, 1
			add bp, offset index_to_y_coor_table
			mov cx, ds:[bp]
			mul cx
			add ax, start_y+padding
			pop cx

			ret 0
__calc_y_offset endp

__should_draw_? proc			; sets carry flag if should NOT draw. expects cx to hold cell id
			push cx
			add cx, 1
			shr ax, cl
			pop cx

			ret 0
__should_draw_? endp


draw_player_x_state proc
			push bp
			mov bp, sp

			mov cx, 0
__loop_x:
			push cx

			mov ax, player_one_map

			call __should_draw_?
			jnc _dont_draw_x			; carry flag remains unchanged by "pop cx"
			call __calc_x_offset
			push ax
			call __calc_y_offset
			push ax
			;; push size
			push inner_width
			call draw_cross_x_y_size

_dont_draw_x:
			pop cx
			inc cx
			cmp cx, 9
			jne __loop_x

			pop bp
			ret 0*2
draw_player_x_state endp


draw_player_y_state proc
			push bp
			mov bp, sp

			mov cx, 0
__loop_y:
			push cx

			mov ax, player_two_map


			call __should_draw_?
			jnc _dont_draw_y			; carry flag remains unchanged by "pop cx"

			;;;; calculate x position.
			call __calc_x_offset
			push ax

			mov bx, ax ; x1 is needed for calculating x2

			;;;; calculate y position.
			call __calc_y_offset
			push ax

			add bx, inner_width			;; x2
			push bx

			add ax, inner_width			;; y2
			push ax
			call draw_rect_x1_y1_x2_y2

_dont_draw_y:
			pop cx
			inc cx
			cmp cx, 9
			jne __loop_y


			pop bp
			ret 0*2
draw_player_y_state endp



draw_board proc
			push bp
			mov bp, sp

			push start_x
			push start_y
			push start_x+sqaure_width 
			push start_y+sqaure_width 
			call draw_rect_x1_y1_x2_y2


			push start_x+half_width
			push start_y
			push start_x+sqaure_width+half_width
			push start_y+sqaure_width 
			call draw_rect_x1_y1_x2_y2


			push start_x+half_width
			push start_y+half_width
			push start_x+sqaure_width +half_width
			push start_y+sqaure_width +half_width
			call draw_rect_x1_y1_x2_y2

			push start_x
			push start_y+half_width
			push start_x+sqaure_width 
			push start_y+sqaure_width +half_width
			call draw_rect_x1_y1_x2_y2

			pop bp
			ret 0
draw_board endp