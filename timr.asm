handle_timer PROC
                                        ; Call  the original interrupt vector
                                        ; by pushing flags register and doing a FAR CALL to old vector
    pushf
    call dword ptr [CS:int1c_old_ofs]
    inc word ptr [CS:timer_ticks]        ; Increase timer counter
    
    iret
handle_timer ENDP



int1c_old_ofs DW 0              ; Offset of original int 1c vector
int1c_old_seg DW 0              ; Segment of original int 1c vector  
timer_ticks DW 0                 ; Timer Tick count
